# LabVANIR #



### What is LabVANIR? ###

**LabVANIR** (**V**erifiable **A**ccess to **N**FTs **I**nstilled with **R**eality) is a collection of digital 3D model **NFT**s (**N**on-**f**ungible **t**okens) 
based on ancient, contemporary, or even fictional devices of scientific or medical design. 